- 3 steps
    
    prepare an input
    call a method
    check an output

- 5 steps
    Setup
    prepare an input
    call a method
    check an output
    Tear down

- Fast 
    frequent execution (tdd, every hours, every day, execute after save)
    execution in groups (execution time become longer)
    good number ?

- Consistent
    multiple invocation should return true or false
    problems: using new LocalDateTime(), ramdom value (random.nextInt())
    solution: mocks, dependency injection

- Atomic
    Only two possibility: pass or fail , no partially successful Tests

- Single responsability
    One test should be responsable of one scenario
    test behaviour, not methods 
    one methods => multiple behaviours => multiple Tests
    one behaviour => multiple methods(a method calls private, protected methods, very simple public method) => one test 
    multiple asserts in the same test => as long as they check the same behaviour

    bad : assert(behaviour1); assert(behaviour2); assert(behaviour3);
    good: testMethodBehaviour1() {assert(behaviour1);}  testMethodBehaviour2() {assert(behaviour2);}  testMethodBehaviour2() {assert(behaviour2);}
    
    bad: behaviour1= condition1 + condition2 + condition3
        behaviour2 = condition4 + condition5
        test all conditions in the same test
    good:
        test in one test condition1 + condition2 + condition3
        and in another test condition4 + condition5

- Tests isolation
    different execution order => the same results
    no state sharing
    instance variable(separated in junit, shared in testng)

- Environment isolation
    should be isolated from the environmental influences
        database access
        webservice calls
        JNDI look up
        Environment variables
        Property files
        System date and time


- Classes isolation
    the less methods are executed by the test, the better (better code maintenance)
    the less tests execute the method the better (better test maintenance)
    but high coverage is a must

    bad: don't call constructors inside a method, use factories or dependency injection
    good: use interface



- Fully automated (no manual steps involved into testing)
    automated tests executions
    automated results gathering 
    automated decision making
    automated results distribution (email, dashboard, IDE integration,...)

- Self-descriptive
    development level documentation
    method specification which is always up to date

    must be easy to read and understand
        variables names
        method names
        class names


- No conditional logic
    no if, switch statements
    no incertainty
        all input values should be known 
        method behaviour should be predictive
        expected output should be strict
    split the test into two (or more) tests instead of adding if or switch statement


- No loops
    avoid hundreds of repetitions => means test is too complicated
    few repetitions
        better to type the code explicitly without loops (extract in methods, code need to be repeat and invoke a few times in a 
        row).
    unkown number of repetitions => your test is incorrect and you should rather focus on specifying more strict input data

        

- No execution catching
    catch an execption only if it is expected
    catch only expected type of an execption
    catch execpted execption and call fail method
    let other execption go uncatched


- Assertions
    use various types of assertions provided by a testing framework
    create your own assertions to check more complicated, repetitive conditions
    reuse your assetions method
    loops inside assertions methods
    loops inside assertions can be a good pratice

- informative assertions messages
    by reading an assertions message only, one should be able to recognize the problem
    it's a good pratice to include business logic information into assertion message.
    assertions messages
        improve documentation of the code
        inform about the problem in case of test failure

- No test logic in production code
    separate unit tests and production code
    don't create methods/fields used only by unit tests
    use dependency injection

- Seperation per business module
    create suite of tests per business module
    use hierachical approach
    decrease the execution time of suites by splitting them into smaller ones (per sub-module)
    small suites can be executed more frequently.

- Seperation per type
    keep unit tests separated from integration tests
    different purpose of execution
    different frequency of execution
    different time of execution
    different action in case of failure

 - tests double(https://dzone.com/articles/test-doubles-mockito)
    dummy
    stub
    mock
    spy
    fake   
    
 - only mock the type you own   



Resources
- Google testing blog
- http://jasonpolites.github.io/tao-of-testing/index-1.1.html#
